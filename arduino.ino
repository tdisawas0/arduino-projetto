This conversation may reflect the link creator’s personalized data, which isn’t shared and can meaningfully change how the model responds.
#define C_open 12 //รีเลย์ทำหน้าที่ควบคุมกระแสไฟจ่ายให้มอเตอร์
#define C_close 13
#define ledPin 6
#define ledPin2 7
#define ledPin3 8
#define ledPin4 9
int curtains=0; //0=เปิด 1=ผ้าม่านปิด
int time=3000;
int analogLightSensor = 5; //ประกาศตัวแปร ให้ analogPin แทนขา analog ขาที่5
int val = 0;
void setup() {
  pinMode(C_open, OUTPUT); // กำหนดโหมดให้เป็น Output
  pinMode(C_close, OUTPUT);
  pinMode(ledPin, OUTPUT); // sets the pin as output
  pinMode(ledPin2, OUTPUT); // sets the pin as output
  pinMode(ledPin3, OUTPUT); // sets the pin as output
  pinMode(ledPin4, OUTPUT);
  Serial.begin(9600);
}
void light(){
  if (val > 600) {
    digitalWrite(ledPin, HIGH);
    digitalWrite(ledPin2, HIGH);
    digitalWrite(ledPin3, HIGH);
    digitalWrite(ledPin4, HIGH);
    //มืดสุดเปิดทุกดวง
  }
  else if(val > 550){
    digitalWrite(ledPin, LOW);
    digitalWrite(ledPin2, HIGH);
    digitalWrite(ledPin3, HIGH);
    digitalWrite(ledPin4, HIGH);
    //มืดรองลงมาเปิด3ดวง
  }
  else if(val > 500){
    digitalWrite(ledPin, LOW);
    digitalWrite(ledPin2, LOW);
    digitalWrite(ledPin3, HIGH);
    digitalWrite(ledPin4, HIGH);
    //มืดรองลงมาเปิด2ดวง
  }
  else if(val > 400) {
    digitalWrite(ledPin, LOW);
    digitalWrite(ledPin2, LOW);
    digitalWrite(ledPin3, LOW);
    digitalWrite(ledPin4, HIGH);
    //เกือบสว่างเปิดดวงเดียว
  }
  else {
    digitalWrite(ledPin, LOW);
    digitalWrite(ledPin2, LOW);
    digitalWrite(ledPin3, LOW);
    digitalWrite(ledPin4, LOW);
    //สว่างปิดทุกดวง
  }
  //ไว้ควบคุมหลอดไฟ
}
void curtains_Close(){
  if(curtains==0){
    curtains=1;
    digitalWrite(C_open, LOW);
    digitalWrite(C_close, HIGH);
    delay(time);
    digitalWrite(C_open, LOW);
    digitalWrite(C_close, LOW);
  }
}
void curtains_Open(){
  if(curtains==1){
    curtains=0;
    digitalWrite(C_open, HIGH);
    digitalWrite(C_close, LOW);
    delay(time);
    digitalWrite(C_open, LOW);
    digitalWrite(C_close, LOW);
  }
}
void curtains_Decision(){
  if(val<100){
    curtains_Close();
    //จ้ามากปิดผ้าม่าน
  }
  else{
    curtains_Open();
  }
}
void loop() {
  int sensorValue = analogRead(A5);
  val = analogRead(analogLightSensor); //อ่านข้อมูลเก็บไว้ใน val
  Serial.print("val = "); // print ค่าออกมาทาง Serial Moniter
  Serial.println(val); // print ค่า val ออกมาดู แสงมืดหรือสว่าง
  light();
  curtains_Decision();
}
